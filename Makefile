#!/bin/sh

CXX = g++

SRC = src
BIN = bin
INP = input
OUT = output
STATEMENT = statement

OLD_OBJ = ${SRC}/newick_parser/seqUtil.o ${SRC}/newick_parser/Newickform.o
OLD_SRC = ${SRC}/mas.cpp ${SRC}/myTree.h
OLD_BIN = ${BIN}/mas.out

GEN_SRC = ${SRC}/generator.cpp
GEN_BIN = ${BIN}/generator.out

PESHO_SRC = ${SRC}/sol_pesho.cpp
PESHO_BIN = ${BIN}/sol_pesho.out

ISKREN_SRC = ${SRC}/sol_iskren.cpp
ISKREN_BIN = ${BIN}/sol_iskren.out

CHECKER_SRC = ${SRC}/checker.cpp
CHECKER_BIN = ${BIN}/checker.out

CMP_SCRIPT = ./src/comparator.py

${OLD_BIN}: ${OLD_SRC} ${OLD_OBJ}
	make -C ${SRC}/newick_parser
	${CXX} ${OLD_SRC} -o ${OLD_BIN} ${OLD_OBJ}

${GEN_BIN}: ${GEN_SRC}
	${CXX} ${GEN_SRC} -o ${GEN_BIN}

${PESHO_BIN}: ${PESHO_SRC}
	${CXX} ${PESHO_SRC} -o ${PESHO_BIN} -O2 -w

${ISKREN_BIN}: ${ISKREN_SRC}
	${CXX} ${ISKREN_SRC} -o ${ISKREN_BIN} -O2 -w

${CHECKER_BIN}: ${CHECKER_SRC}
	${CXX} ${CHECKER_SRC} -o ${CHECKER_BIN} -O2

test: mas ${INP}/35.tree ${INP}/113.tree
	${BIN}/mas.out ${INP}/35.tree ${INP}/113.tree ${OUT}/agreement35and113.tree 2> /dev/null

check: ${CHECKER_BIN}
	${CHECKER_BIN} ${ARGS}
	dot -Tpng ${ARGS}.P.dot -o ${ARGS}.P.png
	dot -Tpng ${ARGS}.Q.dot -o ${ARGS}.Q.png

pesho: ${PESHO_BIN}
	${PESHO_BIN} < ${ARGS}

iskren: ${ISKREN_BIN}
	${ISKREN_BIN} < ${ARGS}

gen: ${CHECKER_BIN} ${GEN_BIN}
	${GEN_BIN} ${ARGS}
	$(MAKE) check ARGS="$(word 1, ${ARGS})"
	$(MAKE) pesho ARGS="$(word 1, ${ARGS})"

genall:
	$(MAKE) gen ARGS="tests/mas.01.in 11 3"
	$(MAKE) gen ARGS="tests/mas.02.in 21 7"
	$(MAKE) gen ARGS="tests/mas.03.in 31 100"
	$(MAKE) gen ARGS="tests/mas.04.in 41 13"
	$(MAKE) gen ARGS="tests/mas.05.in 251 70"
	$(MAKE) gen ARGS="tests/mas.06.in 455 90"
	$(MAKE) gen ARGS="tests/mas.07.in 603 33"
	$(MAKE) gen ARGS="tests/mas.08.in 701 250"
	$(MAKE) gen ARGS="tests/mas.09.in 999 50"
	$(MAKE) gen ARGS="tests/mas.10.in 999 5000"

plot:
	dot -Tpng ${STATEMENT}/dots/${ARGS}.dot -o ${STATEMENT}/pngs/${ARGS}.png
	gnome-open ${STATEMENT}/pngs/${ARGS}.png

compare:
	${CMP_SCRIPT} ${PESHO_BIN} ${ISKREN_BIN} ${INP}/ ${OUT}/

clean:
	rm -f ${BIN}/*
	make -C ${SRC}/newick_parser clean
